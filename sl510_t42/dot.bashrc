
# Check for an interactive session
[ -z "$PS1" ] && return

alias ls='ls -sh --color=auto'
alias pacman='sudo pacman-color'
alias nano='nano -Wx'
# PS1='[\u@\h \W]\$ '
PS1='[\[\033[1;34m\]\u\[\033[0m\]@\h:\w]\$ '

if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi


